#######################################
#
# Copyright Itzel Salvador, August 2020
#
#######################################
import datetime
import sys
import json
import os
from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS, cross_origin
from google.cloud import firestore
from google.cloud import pubsub

# General setups
app = Flask(__name__, static_folder="build/static", template_folder="build")

#Enable CORS for local testing
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

# Get Db client instance
db = firestore.Client()
# Get PubSub reference
publisher = pubsub.PublisherClient()

# Inserts payload data 
def insert_payload(payload_data):
    payload = {
        u'round': payload_data[u'round'],
        u'previous_hand_id': payload_data[u'previousHandID'],
        u'current_hand_id': payload_data[u'currentHandID'],
        u'previous_hand': payload_data[u'previousHand'],
        u'current_hand': payload_data[u'currentHand'],
        u'status' : u'new'
    }

    doc_ref = db.collection(u'game_hands').document()
    doc_ref.set(payload)

    return doc_ref.id

# REST API 
@app.route("/api/save-game-payload", methods=['POST'])
@cross_origin()
def save_game_payload():
    req_data = request.get_json()
    payload_id = insert_payload(req_data)

    # Pub/Sub Trigger Start
    message = {
        u'payload_id': payload_id
    }
    topic_name = 'projects/{}/topics/{}'.format(
        os.getenv('GOOGLE_CLOUD_PROJECT'), 'start-analytics'
    )
    publisher.publish(topic_name, json.dumps(message).encode('utf8'))
    # Pub/Sub Trigger End

    #'Success'
    return_message = 'Call Successful'

    return return_message

@app.route("/api/get-player-prediction", methods=['GET'])
@cross_origin()
def get_prediction_data():

    doc_ref = db.collection(u'game_hand_prediction').document(u'data')
    doc = doc_ref.get()
    prediction_data = doc.to_dict()

    return prediction_data

@app.route('/', methods=['GET'])
def home():
    return 'Home ESG Analytics for Rock Paper Scissors'

#if executed on local
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080)
