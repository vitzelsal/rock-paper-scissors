import sys
import json
import base64
import datetime
from datetime import timedelta, timezone
from flask import escape
from google.cloud import firestore

db = firestore.Client()

def get_hand_prediction():
    game_analytics_ref = db.collection(u'game_analytics').document(u'data')
    game_analytics_doc = game_analytics_ref.get()
    analytics = game_analytics_doc.to_dict()

    hand_prediction = {
        u'next_hand': analytics[u'most_chosen'],
        u'next_hand_id': analytics[u'most_chosen_id'],
        u'previous_hand':[
            {
                u'next_hand': analytics[u'previous_hand'][u'rock'][u'most_chosen'],
                u'next_hand_id': analytics[u'previous_hand'][u'rock'][u'most_chosen_id'],
            },
            {
                u'next_hand': analytics[u'previous_hand'][u'paper'][u'most_chosen'],
                u'next_hand_id': analytics[u'previous_hand'][u'paper'][u'most_chosen_id'],
            },
            {
                u'next_hand': analytics[u'previous_hand'][u'scissors'][u'most_chosen'],
                u'next_hand_id': analytics[u'previous_hand'][u'scissors'][u'most_chosen_id'],
            },
            {
                u'next_hand': analytics[u'previous_hand'][u'lizard'][u'most_chosen'],
                u'next_hand_id': analytics[u'previous_hand'][u'lizard'][u'most_chosen_id'],
            },
            {
                u'next_hand': analytics[u'previous_hand'][u'spock'][u'most_chosen'],
                u'next_hand_id': analytics[u'previous_hand'][u'spock'][u'most_chosen_id'],
            }
        ]
    }

    doc_ref = db.collection(u'game_hand_prediction').document(u'data')
    doc_ref.set(hand_prediction)

def get_game_analytics():
    # Get data from collection
    game_hands_ref = db.collection(u'game_hands')
    query = game_hands_ref.where(u'status', u'==', u'new')
    docs = query.stream()

    # Get data from current analytics
    game_analytics_ref = db.collection(u'game_analytics').document(u'data')
    game_analytics_doc = game_analytics_ref.get()
    analytics = game_analytics_doc.to_dict()

    # Update values for current hand choice
    def update_current_hand_values(current_hand, current_hand_id):
        current_hand_average = current_hand + '_percentage'

        # Increase value for current choice
        analytics[current_hand] += 1
        #Update percentage
        percentage = analytics[current_hand]/analytics[u'total']
        analytics[current_hand_average] =  percentage*100

        # Update max values
        if analytics[current_hand] > analytics[u'max']:
            analytics[u'max'] = analytics[current_hand]
            analytics[u'most_chosen'] = current_hand
            analytics[u'most_chosen_id'] = current_hand_id

        # Update min values
        if analytics[current_hand] < analytics[u'least']:
            analytics[u'least'] = analytics[current_hand]
            analytics[u'least_chosen'] = current_hand
            analytics[u'least_chosen_id'] = current_hand_id

    # Update values for current hand choice according to previous choice
    def update_previous_hand_values(current_hand, current_hand_id, previous_hand, previous_hand_id):
        current_hand_average = current_hand + '_percentage'
        # Increase total and value for current choice
        analytics[u'previous_hand'][previous_hand][u'total'] += 1
        analytics[u'previous_hand'][previous_hand][current_hand] += 1

        # Get percentage of current choice
        percentage = analytics[u'previous_hand'][previous_hand][current_hand]/analytics[u'previous_hand'][previous_hand][u'total']
        analytics[u'previous_hand'][previous_hand][current_hand_average] =  percentage*100

         # Update max values
        if analytics[u'previous_hand'][previous_hand][current_hand] > analytics[u'previous_hand'][previous_hand][u'max']:
            analytics[u'previous_hand'][previous_hand][u'max'] = analytics[u'previous_hand'][previous_hand][current_hand]
            analytics[u'previous_hand'][previous_hand][u'most_chosen'] = current_hand
            analytics[u'previous_hand'][previous_hand][u'most_chosen_id'] = current_hand_id

        # Update min values
        if analytics[u'previous_hand'][previous_hand][current_hand] < analytics[u'previous_hand'][previous_hand][u'least']:
            analytics[u'previous_hand'][previous_hand][u'least'] = analytics[u'previous_hand'][previous_hand][current_hand]
            analytics[u'previous_hand'][previous_hand][u'least_chosen'] = current_hand
            analytics[u'previous_hand'][previous_hand][u'least_chosen_id'] = current_hand_id


    for doc in docs:
        gh_obj = doc.to_dict()

        current_hand = gh_obj[u'current_hand'].lower()
        current_hand_id = gh_obj[u'current_hand_id']
        previous_hand = gh_obj[u'previous_hand'].lower()
        previous_hand_id = gh_obj[u'previous_hand_id']
        
        analytics[u'total'] += 1
        if gh_obj[u'round'] == 1:
            update_current_hand_values(current_hand,current_hand_id)
        else:
            update_current_hand_values(current_hand,current_hand_id)
            update_previous_hand_values(current_hand, current_hand_id, previous_hand, previous_hand_id)

        doc_ref = game_hands_ref.document(doc.id)
        doc_ref.update({u'status': u'counted'})

    game_analytics_ref.set(analytics)

def get_game_stats(event, context):
    get_game_analytics()
    get_hand_prediction()
