﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PayloadData
{
    public int round;
    public string currentHand;
    public int currentHandID;
    public string previousHand;
    public int previousHandID;
}
