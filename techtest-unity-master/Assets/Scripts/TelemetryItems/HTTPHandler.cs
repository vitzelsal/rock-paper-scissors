﻿using System;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class HTTPHandler : MonoBehaviour
{
    [Header("Payload Attributes")]
    [SerializeField] 
    // Time the corutines waits to be executed
    private float _uploadDelay = 0.1f;
    private float _callDelay = 0.1f;

    // Microservice
    private string _urlPOST = "https://eastsidegames-analytics.uk.r.appspot.com/api/save-game-payload";
    private string _urlGET = "https://eastsidegames-analytics.uk.r.appspot.com/api/get-player-prediction";
    
    // Payload
    private PayloadData _payload;
    private bool _sendPayload = false;

     private void Start()
    {
        StartCoroutine(GetPredictionData()); 
        StartCoroutine(Upload()); 
    }

    public void SetPayload(PayloadData payload)
    {
        _payload = payload;
        _sendPayload = true;
    }

    IEnumerator GetPredictionData()
    {
        while(true)
        {
            yield return new WaitForSeconds(_callDelay);
            var request = new UnityWebRequest(_urlGET, "GET");
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                File.WriteAllText(Application.dataPath + "/get_service_logs.json", request.error);
            }
            else
            {
                File.WriteAllText(Application.dataPath + "/prediction_payload.json", request.downloadHandler.text);
            }     
        }
    }

    IEnumerator Upload()
    {
        while(true) 
        { 
            yield return new WaitForSeconds(_uploadDelay);
            
            if(_sendPayload)
            {
                _sendPayload = false;
                var request = new UnityWebRequest(_urlPOST, "POST");
                string jsonPayload = JsonUtility.ToJson(_payload);
                byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(jsonPayload);
                request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
                request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                request.SetRequestHeader("Content-Type", "application/json");
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    File.WriteAllText(Application.dataPath + "/post_service_logs.json", request.error);
                }
                else
                {
                    Debug.Log(request.downloadHandler.text);
                }     
            }
        }
    }
}
