﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CurrentHand
{
    public string next_hand;
    public int next_hand_id;
}

[System.Serializable]
public struct PredictionPayload
{
    public string next_hand;
    public int next_hand_id;
    public List<CurrentHand> previous_hand;
}



