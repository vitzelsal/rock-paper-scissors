﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UpdateGameLoader
{
	public delegate void OnLoadedAction(GameResultsData gameUpdateData);
	public event OnLoadedAction OnLoaded;

	// Delegate use to determine the oponent hand
	public delegate Hand GetOponentAction(Hand hand);
	public event GetOponentAction GetOponentHand;

	// Change from string to Hand object
	private Hand _choice;

	public UpdateGameLoader(Hand playerChoice)
	{
		_choice = playerChoice;
	}

	public void load()
	{
		Hand oponentHand = GetOponentHand(_choice);

		GameResultsData mockGameUpdate = new GameResultsData();
		mockGameUpdate.playerHandValue = _choice.typeName;
		mockGameUpdate.oponentHandValue = oponentHand.typeName;

		Result drawResult = ResultAnalyzer.GetResultState(_choice, oponentHand);
		mockGameUpdate.coinsAmount = (int)drawResult*GameController.betAmount;

		OnLoaded(mockGameUpdate);
	}

}