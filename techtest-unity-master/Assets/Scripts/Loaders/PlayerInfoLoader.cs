﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerInfoLoader
{
	public delegate void OnLoadedAction(PlayerData playerData);
	public event OnLoadedAction OnLoaded;

	public void load()
	{
		// Use of struct for player's data to avoid casting
		PlayerData mockPlayerData = new PlayerData();
		// Call default values for player
		mockPlayerData.Init();
		OnLoaded(mockPlayerData);
	}
}