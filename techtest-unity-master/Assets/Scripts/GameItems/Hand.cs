﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* General Behaviour of Hand */
[CreateAssetMenu(fileName = "DefaultHand", menuName = "GameItem/Hand")]
public class Hand : ScriptableObject
{
    // Define the type of hand: Rock, Paper or Scissors
    public UseableItem type;

    // String equivalent of type
    public string typeName;

    // Defines the won condition for this hand
    public UseableItem[] isStrongerThan = new UseableItem[2];

    // Defines the loose condition for this hand
    public UseableItem[] isWeakerThan = new UseableItem[2];
}
