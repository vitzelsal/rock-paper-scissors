﻿using UnityEngine;
using System.Collections;

// Adding two more options Lizad and Spock
public enum UseableItem
{
	Rock,
	Paper,
	Scissors,
	Lizard,
	Spock
}