﻿using UnityEngine;
using System.Collections;
using System;

// Assigned values according with the effect on the
// player's coins amount
public enum Result
{
	Draw = 0,
	Won = 1,
	Lost = -1,
}

public class ResultAnalyzer
{
	public static Result GetResultState(Hand playerHand, Hand oponentHand)
	{	
		// Checks if the player hand is stronger than oponent hand
		// After adding Lizar and Spock an array is used
		if (Array.Exists(playerHand.isStrongerThan, element => element == oponentHand.type))
		{
			return Result.Won;
		}
		else if (playerHand.type == oponentHand.type)
		{
			return Result.Draw;
		}
		else
		{
			return Result.Lost;
		}
	}
}