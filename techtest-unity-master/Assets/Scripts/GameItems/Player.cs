﻿using UnityEngine;
using System.Collections;
using System;

public class Player
{
	// Attributes
	private int _userId;
	private string _name;
	private int _coins;

	// Getters 
	public int UserId 	{get {return _userId;}}
	public string Name 	{get {return _name;}}
	public int Coins 	{get {return _coins;}}

	public Player(PlayerData playerData)
	{
		// Use specific player data struct to avoid casting 
		_userId = playerData.id;
		_name = playerData.playerName; 
		_coins = playerData.coins;
	}

	public void ChangeCoinAmount(int amount)
	{
		_coins += amount;
	}
}