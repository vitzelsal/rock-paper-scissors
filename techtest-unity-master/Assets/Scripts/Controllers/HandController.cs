﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 0649

// Hand UI wrapper
public class HandController : MonoBehaviour
{
    // Controller Type
    public Hand hand;

    // Button UI Label
    [SerializeField]
    private Text _label;

    // Start is called before the first frame update
    void Start()
    {
        _label.text = hand.typeName;
    }

}
