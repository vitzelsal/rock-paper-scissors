﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0649
public class UIController : MonoBehaviour
{
    [Header ("Player's Components")]
    [SerializeField]
    private Text _nameLabel;
    [SerializeField]
	private Text _moneyLabel;

    [Header ("Hands Components")]
    [SerializeField]
	private Text _playerHand;
    [SerializeField]
	private Text _enemyHand;

    // Updates player's information in UI
    public void UpdatePlayerInfo(PlayerData data)
    {
        _nameLabel.text = data.playerName;
    }

    // Update player hand, oponent hand and money amount
    public void UpdateUI(Dictionary<string, string> data)
    {
        _playerHand.text = data["playerHand"];
		_enemyHand.text = data["oponentHand"];
        _moneyLabel.text = data["money"];
    }
}
