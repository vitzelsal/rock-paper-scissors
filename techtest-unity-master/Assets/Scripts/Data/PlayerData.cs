﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Player's data container
public struct PlayerData 
{
    public int id;
	public string playerName;
	public int coins;

	// Default data for player
	// Used when game starts
	public void Init()
	{
		id = 1;
		playerName = "Player 1";
		coins = 50;
	}
}
