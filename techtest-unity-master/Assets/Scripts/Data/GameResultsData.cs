﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Data container for game results
public struct GameResultsData
{
   public string playerHandValue;
   public string oponentHandValue;
   public int coinsAmount;
}
