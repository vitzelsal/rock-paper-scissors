﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System;

#pragma warning disable 0649

public class GameController : MonoBehaviour
{
	[Header("UI Components")]
	[SerializeField]
	private UIController _uiController;

	[Header("Game Data")]
	[SerializeField] 
	private Hand[] _handOptions;
	private Player _player;

	[Header("Telemetry Components")]
	[SerializeField] 
	private HTTPHandler _httpHandler;
	private PayloadData _payload;
	[SerializeField]
	private bool _prediction = true;

	// Game variables
	// Amount bet every round
	public static int betAmount = 10;
	// Rounds data
	public static int round;

	void Start()
	{	
		// Reset rounds
		round = 0;

		// Create payload container
		_payload = new PayloadData();

		// Load player's data and reflect it in the UI
		LoadPlayerInfo();

	}

	public void LoadPlayerInfo()
	{
		PlayerInfoLoader playerInfoLoader = new PlayerInfoLoader();
		
		// Subscribe
		playerInfoLoader.OnLoaded += OnPlayerInfoLoaded;
		// Call Event
		playerInfoLoader.load();
		// Unsubscribe
		playerInfoLoader.OnLoaded -= OnPlayerInfoLoaded;
	}

	public void OnPlayerInfoLoaded(PlayerData playerData)
	{
		_player = new Player(playerData);
		_uiController.UpdatePlayerInfo(playerData);
	}

	public void HandlePlayerInput(int index)
	{
		UpdateGameLoader updateGameLoader = new UpdateGameLoader(_handOptions[index]);

		// Suscribe: Add methods to delegate list
		updateGameLoader.GetOponentHand += GetEnemyHand;
		updateGameLoader.OnLoaded += OnGameUpdated;

		// Call Event
		updateGameLoader.load();

		// Unsubscribe: Remove methods from delegate list
		updateGameLoader.GetOponentHand -= GetEnemyHand;
		updateGameLoader.OnLoaded -= OnGameUpdated;

		// Increase round value
		round++;
		PostChoice(index);
	}

	private void PostChoice(int choiceIndex)
	{
		string handChoice = _handOptions[choiceIndex].typeName;
		// Set payload data depending on round value
		if (round > 1)
		{
			_payload.previousHandID = _payload.currentHandID;
			_payload.previousHand = _payload.currentHand;
			_payload.currentHandID = choiceIndex;
			_payload.currentHand = handChoice;
		}
		else 
		{
			_payload.previousHandID = -1;
			_payload.previousHand = "none";
			_payload.currentHandID = choiceIndex;
			_payload.currentHand = handChoice;
		}
		_payload.round = round;
		
		// Post payload to service
		_httpHandler.SetPayload(_payload);
	}

	private Hand GetEnemyHand(Hand choiceHand)
	{	
		//Get a random hand index
		int index = 0;

		if(File.Exists(Application.dataPath+"/prediction_payload.json") && _prediction)
        {
            var jsonFile = File.ReadAllText(Application.dataPath+"/prediction_payload.json");
            // Save data in card deck
            PredictionPayload predictionData = JsonUtility.FromJson<PredictionPayload>(jsonFile);

			int nextHandIndex = 0;
			if (round == 1) 
			{
				nextHandIndex = predictionData.next_hand_id;
				index = GetEnemyHandIndex(nextHandIndex);
			}
			else
			{
				int currentHandIndex = (int)choiceHand.type;
				nextHandIndex = predictionData.previous_hand[currentHandIndex].next_hand_id;
				index = GetEnemyHandIndex(nextHandIndex);
			}	        
        }
		else
		{
			index = UnityEngine.Random.Range(0, _handOptions.Length);
			Debug.Log("Random Oponent");
		}

		//Get hand
		Hand enemyHand = _handOptions[index];
		return enemyHand;
	}

	public int GetEnemyHandIndex(int nextHandIndex)
	{
		Hand nextHand = _handOptions[nextHandIndex];
		int oponentHandIndex = UnityEngine.Random.Range(0, nextHand.isWeakerThan.Length);
		int index = (int)nextHand.isWeakerThan[oponentHandIndex];
		return index;
	}

	public void OnGameUpdated(GameResultsData gameUpdateData)
	{
		// Update player's coins
		_player.ChangeCoinAmount(gameUpdateData.coinsAmount);

		// Update UI
		// Use of Generics instead of Hashtable. Avoids casting 
		Dictionary<string,string> uiData = new Dictionary<string, string>();
		uiData["playerHand"] = gameUpdateData.playerHandValue;
		uiData["oponentHand"] = gameUpdateData.oponentHandValue;
		uiData["money"] = _player.Coins.ToString();
		_uiController.UpdateUI(uiData);
	}
}

